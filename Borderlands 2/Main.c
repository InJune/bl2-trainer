#include <stdio.h>
#include "Functions.h"

MemStruct cash_mem = { 0x01E961B8, 5, { 0x2BC, 0x29C, 0x5FC, 0x3EC, 0x2A0 } };

int main()
{
	HWND hWnd = FindWindowA(NULL, "Borderlands 2 (32-bit, DX9)");
	if (!hWnd)
	{
		printf("Failed to find Borderlands 2.\n");
		return 1;
	}
	printf("Borderlands 2: found!\n");

	DWORD dwProcessId;
	GetWindowThreadProcessId(hWnd, &dwProcessId);
	if (!dwProcessId)
	{
		printf("Failed to get process ID for Borderlands 2.\n");
		return 1;
	}
	printf("Borderlands 2: got process id! (%d)\n", dwProcessId);

	HANDLE hHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessId);
	if (!hHandle)
	{
		printf("Failed to get handle.\n");
		return 1;
	}
	printf("Borderlands 2: got handle!\n");

	DWORD moduleAddress = get_module_base_address(hHandle);
	if (!moduleAddress)
	{
		printf("Failed to get base module adress. \n");
		return 1;
	}
	printf("Borderlands 2: got base module address! (0x%08X)\n", moduleAddress);

	int new_val = 0;
	while (new_val != -1)
	{
		printf("Enter new money value (current %d): ", read(hHandle, moduleAddress, &cash_mem));
		scanf_s("%d", &new_val);

		write(hHandle, moduleAddress, &cash_mem, new_val);

		printf("\n");
	}
	return 0;
}