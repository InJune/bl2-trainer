#include "Functions.h"

DWORD process_offsets(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address)
{
	DWORD actual_address = baseModuleAddress + address->base_address;
	ReadProcessMemory(hProcHandle, (LPCVOID)actual_address, &actual_address, sizeof(actual_address), NULL);

	for (int i = 0; i < address->size; i++)
	{
		actual_address += address->offsets[i];
		if (i != (address->size) - 1)
			ReadProcessMemory(hProcHandle, (LPCVOID)actual_address, &actual_address, sizeof(actual_address), NULL);
	}
	return actual_address;
}

void write(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address, int value)
{
	WriteProcessMemory(hProcHandle, (LPVOID)process_offsets(hProcHandle, baseModuleAddress, address), &value, sizeof(value), NULL);
}

int read(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address)
{
	int read;
	ReadProcessMemory(hProcHandle, (LPVOID)process_offsets(hProcHandle, baseModuleAddress, address), &read, sizeof(read), NULL);
	return read;
}

DWORD get_module_base_address(HANDLE hProcess)
{
	HMODULE hMods[1024];
	DWORD cbNeeded;
	if (EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
	{
		for (unsigned int i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
		{
			TCHAR szModName[MAX_PATH];
			if (GetModuleBaseName(hProcess, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
			{
				if (strcmp("Borderlands2.exe", szModName) == 0)
				{
					return (DWORD)hMods[i];
				}
			}
		}
	}
	return 0;
}