#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#include <Windows.h>
#include <Psapi.h>

// This is how we store the base address of the memory location we wish to edit
// along with all of its offsets. It requires the number of offsets because I
// couldn't figure out how to generate dynamically.
//
// It should be something like sizeof(offsets)/sizeof(offsets[0]) but then
// it says it is an incomplete type.
typedef struct memory_struct {
	DWORD base_address;
	int size;
	DWORD offsets[];
} MemStruct;

int read(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address);
void write(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address, int value);
DWORD process_offsets(HANDLE hProcHandle, DWORD baseModuleAddress, MemStruct *address);

DWORD get_module_base_address(HANDLE hProcess);
#endif